#smartpost
该项目是一个HTTP请求代理工具，可以实现录入请求信息管理，请求组合，将会自动保存请求服务返回的cookie
当组合多个请求的时候，如何请求返回的cookie信息将会在后面的请求中返回给服务端。
是为了方便给restful接口进行测试而设计开发的。当前只满足基本功能，后续将会继续再次基础上面进行完善。

#主要功能介绍
##基础信息的录入
该部分功能主要是将请求信息录入到smartpost中，录入的信息包括http请求头部信息，请求方式POST/GET以及请求的参数。
###请求参数设置说明
smartpost对请求参数支持form表达提交的数据，该参数是键值对的形式，如果是POST那么将会以key1=value1的形式提交到服务器端，如果是GET形式，则键值对参数追加到请求地址后面。除了form表单参数提交外，还支持json，xml,html以及text的格式参数。如果选中json，那么发送给服务器端的Content-type就是application/json;charset=UTF-8,依次类推：xml则是application/xml;charset=UTF-8;text:text/plain;charset=UTF-8;html:text/html;charset=UTF-8。所以选中非form的参数类型以后，不需要再在header里面设置Content-type属性，除非你有特殊的需求。

##场景
我们在开发过程中，一般为了调试一个接口，可能需要发起多个url请求，比如一个查看商品的流程：<br>
1. 首先需要打开电商网站的首页<br>
2. 查看商品<br>

这里涉及到至少两个个url的请求（首页，商品）。而且这两个请求在smartpost里面可以组合成一个场景，可以叫做【查看商品】。你可以执行该场景，那么将会依照设定的URL请求循序来执行请求，并返回执行结果。

smartpost在这里所做的只是一个请求的组合。但是这样还不够，可以看到上面上个请求存在一定的关系。我这里先假设这个电商网站的地址是（假设这个请求叫做request_index）
> http://www.smpmall.com

上面是个首页的地址，打开首页将会看到很多的商品。我们一般会选择一个商品，我不如再假设该电商网站首页布局是这样的：

    <!-- lang: html -->
     <html>
     <head>
     <title>首页</title>
     </head> 
     <body>
            <div id="items">
                <div class="item">
                        <img src="xxxx.jpg">
                        <a href="item-1222.html"></a>
                </div>
                <div class="item">
                        <img src="xxxx.jpg">
                        <a href="item-1223.html"></a>
                </div>
            </div>
     </body>
     </html>

那么假设我选择上面item-1222.html，在smartpost里面怎么做额？
只需要将这个请求定义为(假设这个请求叫做request_item)

> http://www.smpmall.com/#{html:'#items .item a&[attr:href]'[0]}

当这个请求发生在request_index之后，那么将会从request_index请求返回的html里面解析出item-1222.html这个内容替换上面#{html:'#items .item a&[attr:href]'[0]}表达式。

这里就是smartpost的存在上下文的场景，这也是smartpost设定场景的必要性。基于这个，开发可以对整个业务流程进行测试。下面将对上下文的占位符配置进行描述：

##引用占位符配置
smartpost的占位符配置主要分文几个部分
>  #{type:'expression'[index][default:defaultvalue]}<br>

其中type可以是：html,json,header,cookie<br>
expression则是当前类型的匹配规则，比如type为header的时候，此时的expression则是header中的key值<br>
[index]是可选配置，表示如果通过expression匹配了多个，则只取指定的第几个<br>
[defalut:defaultvalue]也是可选配置，表示当没有匹配到制定的expression的值时，默认值是多少<br>
下面主要对各个类型的expression进行描述

###1、type为header时
这个上面已经例举了，当type为header的时候，expression表示是header的key值，将会将key对应的value替换该占位符


###2、type为cookie时

和为header类似，只是从返回的cookie里面取值

###3、type为json时

此时expression是一个JSONPATH表达式，具体规则可以到这里看看 
https://github.com/jayway/JsonPath

###4、type为html时

这里的expression包含两个部分前面是一个css选择器，用于指定html元素，另一个是这个元素的哪个值。所以是这个结构：cssselector&[text|val|attr:attrName]<br>
关于前面的cssselector这里就不说了，应该都知道，说一下后面的&[text|val|attr:attrName]<br>
比如上面的#{html:'#items .item a&[attr:href]'[0]}表示#items .item a CSS选择器匹配的元素第0个元素属性为href的值。只有为attr的时候需要指定是哪个属性的值。

