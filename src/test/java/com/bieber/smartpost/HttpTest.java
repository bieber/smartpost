package com.bieber.smartpost;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by bieber on 2015/6/8.
 */
public class HttpTest {
    
    
    public static void main(String[] args) throws IOException {
        URL url = new URL(args.length>0?args[0]:"http://192.168.1.158:8080/mtp-web/p1/op_login.htm");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.addRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1653.0 Safari/537.36");
        connection.addRequestProperty("Content-type","application/json;charset=UTF-8");
        connection.setDoOutput(true);
        OutputStream outputStream = connection.getOutputStream();
        String content ="{" +
                "\"clientId\" : \"IOS4\"," +
                "\"appId\" : \"100001\"," +
                "\"operationType\" : \"op_login\"," +
                "\"sessionId\" : \"0beb59d0-f041-11e2-9046-83654c2b2fab\"," +
                "\"loginId\" : \"18616206996\"," +
                "    \"syncMSD\" : \"N\"," +
                "\"loginPassword\" : \"MTxAAAAAACxOaidYk90RoH2DWr2iYEMlkNBc/NaADzZ2KB8V3WFRX8sckxeCxTPrMp5JrwbnVxXT+c1kT1smGzB62qgrY9LD8KFfLVztyxwaxuHPNvUaXdTpSjGS7OdP01LwnwZ6w2lFgQNrXoDe0V/oQTG6V2waLABX+mh5+HBiYcFfQEQJkfNp+h+yftQNljlEQAyNzAAAAAAAHY5dSmTPWYk/gPdjKV7rJWBnCDccUbpcJOTb9GWx00iltVm98VjSSf5o/qh7E+TwoTMfT0f6eRAvMGJA1VRhpcw5WS3geSVenXQb8oi7H1FHRaT/pLHd4T0FzrDFBn/Jk2/SNhD/fZIhDXlRovRqR2WYSyu+tbOoqfxuT1l9bmpVWQhovD2GBWyd9K3GlTXE2+QyKNG2oOrrlitegipUDssRTxjMum4lXYPz4+kGiIR8rPn6FWyKfbhgcqe8UuDmM41KXxT/+hdwT3XmzTjpUdMq9ITOMxes6JtoXjo6HLE+jF1/kI1bQpYfO6i16WgklAnB09UksV2WU9aJbIHAh6c47GMwwszhZOqGUHk0A==\"" +
                "}";
        outputStream.write(content.getBytes());
        outputStream.flush();
        Map<String,List<String>> headers =  connection.getHeaderFields();
        for(Map.Entry<String,List<String>> entry:headers.entrySet()){
            System.out.println(entry.getKey()+"="+entry.getValue());
        }
        InputStream inputStream = connection.getInputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int offset = -1;
        while((offset=inputStream.read(buffer,0,1024))>0){
            byteArrayOutputStream.write(buffer,0,offset);
        }
        System.out.println(new String(byteArrayOutputStream.toByteArray()));
        connection.connect();
    }
}
