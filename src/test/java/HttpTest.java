
/**
 * Created by bieber on 2015/6/8.
 */
public class HttpTest {
    
    
    public static void main(String[] args) throws Exception {
        Request request = new Request(args.length>0?args[0]:"http://192.168.1.158:8080/mtp-web/p1/op_login.htm","json");
        String content ="{" +
                "\"clientId\" : \"IOS4\"," +
                "\"appId\" : \"100001\"," +
                "\"operationType\" : \"op_login\"," +
                "\"sessionId\" : \"0beb59d0-f041-11e2-9046-83654c2b2fab\"," +
                "\"loginId\" : \"18616206996\"," +
                "    \"syncMSD\" : \"N\"," +
                "\"loginPassword\" : \"MTxAAAAAACxOaidYk90RoH2DWr2iYEMlkNBc/NaADzZ2KB8V3WFRX8sckxeCxTPrMp5JrwbnVxXT+c1kT1smGzB62qgrY9LD8KFfLVztyxwaxuHPNvUaXdTpSjGS7OdP01LwnwZ6w2lFgQNrXoDe0V/oQTG6V2waLABX+mh5+HBiYcFfQEQJkfNp+h+yftQNljlEQAyNzAAAAAAAHY5dSmTPWYk/gPdjKV7rJWBnCDccUbpcJOTb9GWx00iltVm98VjSSf5o/qh7E+TwoTMfT0f6eRAvMGJA1VRhpcw5WS3geSVenXQb8oi7H1FHRaT/pLHd4T0FzrDFBn/Jk2/SNhD/fZIhDXlRovRqR2WYSyu+tbOoqfxuT1l9bmpVWQhovD2GBWyd9K3GlTXE2+QyKNG2oOrrlitegipUDssRTxjMum4lXYPz4+kGiIR8rPn6FWyKfbhgcqe8UuDmM41KXxT/+hdwT3XmzTjpUdMq9ITOMxes6JtoXjo6HLE+jF1/kI1bQpYfO6i16WgklAnB09UksV2WU9aJbIHAh6c47GMwwszhZOqGUHk0A==\"" +
                "}";
        Request.Response response = request.post(content);
        System.out.println(response);
    }
}
