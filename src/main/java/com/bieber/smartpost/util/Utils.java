package com.bieber.smartpost.util;

import com.jbeer.framework.logging.Log;
import com.jbeer.framework.logging.LogFactory;
import com.jbeer.framework.utils.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by bieber on 14-10-26.
 */
public class Utils {

    public static final Log COMMONS_LOGGER = LogFactory.getLog("SMARTPOST");

    private static final String COOKIE_RESERVED_FIELD="version";

    private static final String SMART_POST_REMOTE_COOKIE_TOKEN="SMART_POST_REMOTE_COOKIE_TOKEN_";

    public static List<HttpCookie> getCookies(HttpServletRequest request){
        List<HttpCookie> cookieList = new ArrayList<HttpCookie>();
        StringBuffer cookiesStr = new StringBuffer();
        Cookie[] cookies = request.getCookies();
        if(cookies==null){
            return cookieList;
        }
        String cookieValue=null;
        for(Cookie cookie:cookies){
            if(cookie.getName().indexOf(SMART_POST_REMOTE_COOKIE_TOKEN)<0){
                continue;
            }
            HttpCookie cookieProxy = new HttpCookie(cookie.getName().replace(SMART_POST_REMOTE_COOKIE_TOKEN, ""),cookie.getValue());
           /* //cookieProxy.setDomain(cookie.getDomain());
            cookieValue = cookie.getValue();
            if(!cookieValue.startsWith("\"")&&!cookieValue.endsWith("\"")){
                cookieValue="\""+cookieValue+"\"";
            }
            cookieProxy.setValue(cookieValue);*/
            cookieList.add(cookieProxy);
        }
        return cookieList;
    }

    public static String formatExceptionContent(Throwable throwable){
        StringBuffer content = new StringBuffer();

        content.append(throwable.getClass().getName()).append(":").append(throwable.getMessage()).append("\n");
        StackTraceElement[] stackTraceElements = throwable.getStackTrace();
        for(StackTraceElement element:stackTraceElements){
            content.append("&nbsp;&nbsp;").append(element.getClassName()).append(":").append(element.getMethodName()).append("[").append(element.getLineNumber()).append("]\n");
        }
        if(throwable.getCause()!=null){
            content.append("case by ").append(formatExceptionContent(throwable.getCause()));
        }
        return content.toString();
    }

    public static void setCookies(HttpServletResponse response, Collection<HttpCookie> proxyCookies){
            if(proxyCookies==null){
                return ;
            }
          if(proxyCookies.size()>0){
              StringBuilder temp = new StringBuilder();
              for(HttpCookie cookie:proxyCookies){
                  temp.append(SMART_POST_REMOTE_COOKIE_TOKEN).append(cookie.getName());
                  Cookie responseCookie = new Cookie(temp.toString(),cookie.getValue());
                  if(cookie.getMaxAge()>0) {
                      responseCookie.setMaxAge(Integer.parseInt(cookie.getMaxAge()+""));
                  }
                  //responseCookie .setHttpOnly(true);
                  response.addCookie(responseCookie);
                  temp.setLength(0);//reset string builder
              }
          }
    }
}
