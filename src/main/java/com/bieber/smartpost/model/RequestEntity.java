package com.bieber.smartpost.model;

import java.util.List;
import java.util.Map;

/**
 * Created by bieber on 14-10-26.
 */
public class RequestEntity {

    private String entityId;

    private String name;

    private String url;

    private String type;

    private String group;

    private String parametersType;

    private Object params;

    private int invokeTime=1;

    private long intervalTime=0;

    private String cookies;

    private String envId;

    private String clientIp;


    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    private List<Map<String,String>> header;

    public String getEnvId() {
        return envId;
    }

    public void setEnvId(String envId) {
        this.envId = envId;
    }

    public int getInvokeTime() {
        return invokeTime;
    }

    public void setInvokeTime(int invokeTime) {
        this.invokeTime = invokeTime;
    }

    public long getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(long intervalTime) {
        this.intervalTime = intervalTime;
    }

    public String getCookies() {
        return cookies;
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }

    public String getParametersType() {
        return parametersType;
    }

    public void setParametersType(String parametersType) {
        this.parametersType = parametersType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }

    public List<Map<String, String>> getHeader() {
        return header;
    }

    public void setHeader(List<Map<String, String>> header) {
        this.header = header;
    }

    @Override
    public String toString() {
        return "RequestEntity{" +
                "entityId='" + entityId + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", type='" + type + '\'' +
                ", group='" + group + '\'' +
                ", parametersType='" + parametersType + '\'' +
                ", params=" + params +
                ", invokeTime=" + invokeTime +
                ", intervalTime=" + intervalTime +
                ", cookies='" + cookies + '\'' +
                ", envId='" + envId + '\'' +
                ", clientIp='" + clientIp + '\'' +
                ", header=" + header +
                '}';
    }
}
