/**
 * 
 */
package com.bieber.smartpost;

import com.jbeer.framework.config.*;

/**
 * @author bieber
 *
 */
public class AppConfig implements Configurator {


    public void configContext(JBeerConfig jBeerConfig) {

    }

    public void configAop(AopConfig aopConfig) {

    }

    public void configDB(DBConfig dbConfig) {

    }

    public void configWeb(WebConfig webConfig) {

    }

    public void configIOC(IOCConfig iocConfig) {

    }

    public void configIN18(IN18Config in18Config) {

    }

    public void configProperties(PropertiesConfig propertiesConfig) {
        propertiesConfig.setPropertiesPath("permission/permission.properties");
    }

    public void pluginConfig(PluginConfigHandler pluginConfigHandler) {

    }
}
