package com.bieber.smartpost.controller;

import com.jbeer.framework.annotation.Action;
import com.jbeer.framework.annotation.Controller;
import com.jbeer.framework.exception.JBeerException;
import com.jbeer.framework.web.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bieber on 2014/12/4.
 */
@Controller(urlPattern = "/test")
public class TestController {

    @Action(urlPatterns = "/index.htm")
    public ModelAndView index(){
        Map<String,String> params = new HashMap<String,String>();
        params.put("name","bieber");
        params.put("age","34");
        ModelAndView.JSONModelAndView jmav = ModelAndView.createJsonModel();
        jmav.setData(params);
        return jmav;
    }
    @Action(urlPatterns = "/validate.htm")
    public ModelAndView validate(HttpServletRequest request) throws IOException, JBeerException {
       InputStream is =  request.getInputStream();
       ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
       byte[] buffer = new byte[1024];
       int offset=-1;
        while((offset=is.read(buffer,0,1204))>0){
            byteArrayOutputStream.write(buffer,0,offset);
        }
        ModelAndView.JSONModelAndView jmav = ModelAndView.createJsonModel();
        jmav.setData(new String(byteArrayOutputStream.toByteArray()));
        return jmav;
    }
}
