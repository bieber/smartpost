package com.bieber.smartpost.invoke.filter;

import com.bieber.smartpost.invoke.CookieContainer;
import com.bieber.smartpost.invoke.FilterChain;
import com.bieber.smartpost.model.RequestEntity;
import com.jbeer.framework.annotation.Bean;

/**
 * Created by bieber on 14-11-30.
 */
@Bean
public class WeaveCookieFilter extends AbstractFilter {
    public void doFilter(FilterChain chain, RequestEntity entity) {
        entity.setCookies(CookieContainer.getCookiesForCurrentInvoke(chain.getContext().getInvokeTask().getInvokeId()));
        chain.doChain(entity);
    }

    public int order() {
        return 1;
    }
}
