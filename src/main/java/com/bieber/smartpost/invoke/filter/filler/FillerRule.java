package com.bieber.smartpost.invoke.filter.filler;

/**
 * Created by bieber on 14-11-30.
 */
public class FillerRule {

    public RuleType ruleType;

    public String expression;

    public Integer index;

    public String defaultValue;

    public FillerRule(RuleType ruleType, String expression, Integer index,String defaultValue) {
        this.ruleType = ruleType;
        this.expression = expression;
        this.index = index;
        this.defaultValue=defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public String getExpression() {
        return expression;
    }

    public Integer getIndex() {
        return index;
    }

    static enum  RuleType{
        JSON("json"),HTML("html"),HEADER("header"),COOKIE("cookie");

        String value;

         RuleType(String value) {
             this.value = value;
        }

         static RuleType getType(String value){
             RuleType[] ruleTypes =  RuleType.class.getEnumConstants();
             for(RuleType type:ruleTypes){
                 if(type.value.equals(value)){
                     return type;
                 }
             }
             return null;
        }
    }

    @Override
    public String toString() {
        return "FillerRule{" +
                "ruleType=" + ruleType +
                ", expression='" + expression + '\'' +
                ", index=" + index +
                ", defaultValue='" + defaultValue + '\'' +
                '}';
    }
}
