package com.bieber.smartpost.invoke.filter;

import com.bieber.smartpost.invoke.Filter;

/**
 * Created by bieber on 14-11-30.
 */
public abstract class AbstractFilter implements Filter {


    public int compareTo(Filter o) {
        if(Filter.class.isAssignableFrom(o.getClass())) {
            Filter otherFilter = (Filter) o;
            return this.order() - otherFilter.order();
        }
        return -1;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
