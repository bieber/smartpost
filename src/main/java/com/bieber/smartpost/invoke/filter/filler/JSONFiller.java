package com.bieber.smartpost.invoke.filter.filler;

import com.bieber.smartpost.invoke.FilterContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.jbeer.framework.annotation.Bean;
import com.jbeer.framework.exception.JBeerException;
import com.jbeer.framework.parser.json.JSON;
import com.jbeer.framework.utils.StringUtils;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 处理#{json:'jsonPathExpression'[number][default:xxxxx]}占位符的参数
 * <a href="https://github.com/jayway/JsonPath">JsonPath</a>
 * Created by bieber on 14-11-30.
 */
@Bean
public class JSONFiller extends  BasicFiller<String> {


    private static final String EXPRESSION_REGEX="'\\$[\\x20\\t\\r\\n\\f\\w\\?\\.@\\[\\]\\(\\)<=\\$'-:,\\*]+'";

    private static final Pattern JSON_FILLER_PATTERN = Pattern.compile("#\\{json:("+EXPRESSION_REGEX+INDEX_REGEX+"?)"+DEFAULT_VALUE_REGEX+"\\}");
    @Override
    protected String getExpressionRegex() {
        return EXPRESSION_REGEX;
    }

    protected String convertValue(String originValue,String jsonString) throws JBeerException {
        ReadContext docContext = JsonPath.parse(jsonString);
        List<FillerRule> rules =  generateFillerRule(JSON_FILLER_PATTERN,originValue);
        if(rules==null||rules.size()==0){
            return originValue;
        }
        String[] args = new String[rules.size()];
        for(int i=0;i<rules.size();i++){
            FillerRule rule = rules.get(i);
            try{
                if(rule.getIndex()!=null){
                    List<Object> jsonArray = docContext.read(rule.getExpression());
                    Object jsonObject = jsonArray.get(rule.getIndex());
                    if(Map.class.isAssignableFrom(jsonObject.getClass())){
                        args[i]= JSON.writeToJson(jsonObject);
                    }else{
                        args[i]=jsonObject.toString();
                    }
                }else{
                    Object jsonObject = docContext.read(rule.getExpression());
                    if(Map.class.isAssignableFrom(jsonObject.getClass())){
                        args[i]= JSON.writeToJson(jsonObject);
                    }else{
                        args[i]=jsonObject.toString();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                args[i]="\"\"";
            }
        }
        originValue = JSON_FILLER_PATTERN.matcher(originValue).replaceAll("%s");
        return String.format(originValue,args);
    }

    @Override
    protected String generateDataSource(FilterContext context) throws IOException, JBeerException {
        return context.getLastResponse().getResponseContent();
    }

    public static void main(String[] args) throws JBeerException {
        String content="{\n" +
                "\"name\":\"#{json:'$.persion[0].name'}\",\n" +
                " \"age\":\"#{json:'$.persion[0].age'}\"\n" +
                "}";

        String value = "{\n" +
                "  \"persion\": [\n" +
                "    {\n" +
                "      \"name\": \"bieber\", \n" +
                "      \"age\": \"33\"\n" +
                "    }, \n" +
                "    {\n" +
                "      \"name\": \"bieber2\", \n" +
                "      \"age\": \"34\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        System.out.println(value.trim());
        System.out.println(JSON.writeToJson(JSON.readOriginJsonData(value)));
        value=JSON.writeToJson(JSON.readOriginJsonData(value));
        System.out.println(JSON_FILLER_PATTERN.pattern());
        Matcher matcher = Pattern.compile(EXPRESSION_REGEX).matcher(content);
        //System.out.println(matcher.matches());
        while(matcher.find()){
            System.out.println("matcher:"+matcher.group());
        }

        JSONFiller jsonFiller = new JSONFiller();
        System.out.println(jsonFiller.convertValue(content,value));

    }
}
