package com.bieber.smartpost.invoke;

import com.bieber.smartpost.httpcore.Response;

/**
 * Created by bieber on 14-11-30.
 */
public class FilterContext {

    private Response lastResponse;

    private HttpInvokeTask invokeTask;

    public HttpInvokeTask getInvokeTask() {
        return invokeTask;
    }

    public void setInvokeTask(HttpInvokeTask invokeTask) {
        this.invokeTask = invokeTask;
    }

    public Response getLastResponse() {

        return lastResponse;
    }

    public void setLastResponse(Response lastResponse) {
        this.lastResponse = lastResponse;
    }
}
