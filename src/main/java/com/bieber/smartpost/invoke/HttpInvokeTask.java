package com.bieber.smartpost.invoke;

import com.bieber.smartpost.model.RequestEntity;

import java.util.List;
import java.util.UUID;

/**
 * Created by bieber on 2014/11/6.
 */
public class HttpInvokeTask {

    private List<RequestEntity> entities;
    private String invokeId;

    public HttpInvokeTask(List<RequestEntity> entities){
        this.entities = entities;
        this.invokeId = UUID.randomUUID().toString();
    }

    public List<RequestEntity> getEntities() {
        return entities;
    }

    public String getInvokeId() {
        return invokeId;
    }

}
