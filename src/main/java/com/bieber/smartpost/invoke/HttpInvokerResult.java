package com.bieber.smartpost.invoke;

import com.bieber.smartpost.httpcore.Header;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bieber on 2014/11/6.
 */
public class HttpInvokerResult {

    private String header;

    private String responseContent;

    private String responseCode;

    private long expendTime;

    private int index;

    private String name;

    private String contentType;

    private Header responseHeader;

    private String entityId;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        invokeMetadata.put("entityId",entityId);
        this.entityId = entityId;
    }

    private Map<String,Object> invokeMetadata;

    public Map<String, Object> getInvokeMetadata() {
        return invokeMetadata;
    }

    public HttpInvokerResult() {
        invokeMetadata=new HashMap<String,Object>();
    }

    public Header getResponseHeader() {
        return responseHeader;
    }

    public void setResponseHeader(Header responseHeader) {
        this.responseHeader = responseHeader;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        invokeMetadata.put("name",name);
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        invokeMetadata.put("contentType",contentType);
        this.contentType = contentType;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        invokeMetadata.put("index",index);
        this.index = index;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        invokeMetadata.put("header",header);
        this.header = header;
    }

    public String getResponseContent() {
        return responseContent;
    }

    public void setResponseContent(String responseContent) {
        invokeMetadata.put("responseContent",responseContent);
        this.responseContent = responseContent;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        invokeMetadata.put("responseCode",responseCode);
        this.responseCode = responseCode;
    }

    public long getExpendTime() {
        return expendTime;
    }

    public void setExpendTime(long expendTime) {
        invokeMetadata.put("expendTime",expendTime);
        this.expendTime = expendTime;
    }

    @Override
    public String toString() {
        return "HttpInvokerResult{" +
                "header='" + header + '\'' +
                ", responseContent='" + responseContent + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", expendTime=" + expendTime +
                ", index=" + index +
                ", contentType='" + contentType + '\'' +
                '}';
    }
}
