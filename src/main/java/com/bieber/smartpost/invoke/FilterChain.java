package com.bieber.smartpost.invoke;

import com.bieber.smartpost.invoke.filter.InvokerFilter;
import com.bieber.smartpost.model.RequestEntity;
import com.jbeer.framework.exception.InitializationException;
import com.jbeer.framework.ioc.JBeerIOCContainerContext;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by bieber on 2014/11/12.
 */
public class FilterChain {

    private LinkedList<Filter> filterList;

    private FilterContext context;


    private static final InvokerFilter invokerFilter = new InvokerFilter();

    public FilterChain(FilterContext context) throws InitializationException {
        Collection<Filter> filters = JBeerIOCContainerContext.getBeansByType(Filter.class).values();
        filterList = new LinkedList<Filter>(filters);
        Collections.sort(filterList);
        this.context = context;
    }


    public void doChain(RequestEntity entity){
        if(filterList==null||filterList.size()<=0){
            return ;
        }
        filterList.pop().doFilter(this, entity);
    }

    public FilterContext getContext() {
        return context;
    }
}
