package com.bieber.smartpost.invoke;

import com.bieber.smartpost.model.RequestEntity;

/**
 * Created by bieber on 2014/11/12.
 */
public interface Filter extends Comparable<Filter>{

    public void doFilter(FilterChain chain,RequestEntity entity);

    public int order();
}
