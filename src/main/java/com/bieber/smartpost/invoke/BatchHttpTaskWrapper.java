package com.bieber.smartpost.invoke;

import com.bieber.smartpost.httpcore.Response;
import com.bieber.smartpost.model.RequestEntity;

/**
 * Created by bieber on 2014/11/6.
 */
public interface BatchHttpTaskWrapper {

    public abstract void wrap(Response response,RequestEntity entity,String invokeId );


}
