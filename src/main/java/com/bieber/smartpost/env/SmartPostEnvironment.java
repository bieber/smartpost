package com.bieber.smartpost.env;

import com.bieber.smartpost.util.Utils;
import com.jbeer.framework.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Pattern;

/**
 * Created by bieber on 14-11-10.
 */
public abstract class SmartPostEnvironment extends ReentrantReadWriteLock{

    protected Map<String,String> container;

    protected  EnvType type;

    protected static Pattern ENV_KEY_PLACEHOLDER_PATTERN = Pattern.compile("^\\$\\{[^\\s]{1,}\\}$");

    static enum  EnvType{
        GLOBAL,CUSTOM
    }

    protected SmartPostEnvironment(EnvType type){
         this.type = type;
         this.container = new HashMap<String, String>();
    }

    public void put(String key,String value){
        Lock lock = writeLock();
        try {
            lock.lockInterruptibly();
        } catch (InterruptedException e) {
            Utils.COMMONS_LOGGER.debug("lock exception ",e);
        }
        try{
            container.put(key,value);
        }finally {
            lock.unlock();
        }
    }

    public String get(String key){
        Lock lock  = readLock();
        try {
            lock.lockInterruptibly();
        } catch (InterruptedException e) {
            Utils.COMMONS_LOGGER.debug("lock exception ", e);
        }
        try{
            String realKey=null;
            if(ENV_KEY_PLACEHOLDER_PATTERN.matcher(key).matches()){
                realKey=key.substring(2,key.length()-1);
            }
            if(StringUtils.isEmpty(realKey)){
                return "";
            }
            return container.get(realKey);
        }finally {
            lock.unlock();
        }
    }

    public void clear(){
        Lock lock = writeLock();
        try {
            lock.lockInterruptibly();
        } catch (InterruptedException e) {
            Utils.COMMONS_LOGGER.debug("lock exception ", e);
        }
        try{
            container.clear();
        }finally {
            lock.unlock();
        }
    }



}
