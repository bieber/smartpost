package com.bieber.smartpost.env;

import com.jbeer.framework.exception.JBeerException;
import com.jbeer.framework.parser.json.JSON;
import com.jbeer.framework.utils.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by bieber on 14-11-10.
 */
public class EnvironmentFacade {


    private static GlobalEnv GLOBAL_ENV=new GlobalEnv();

    private static ConcurrentHashMap<String,CustomEnv> CUSTOM_ENV_MAPPING=new ConcurrentHashMap<String, CustomEnv>();

    private static final String GLOBAL_ENV_TOKEN="globalEnv";

    public static void refreshEnv(String envJson) throws JBeerException {
        if(StringUtils.isEmpty(envJson)){
            return;
        }
        Map<String,Object> jsonMap = (Map<String, Object>) JSON.readOriginJsonData(envJson);
        if(jsonMap==null){
            return ;
        }
        for(Map.Entry<String,Object> entry:jsonMap.entrySet()){
            Map<String,Object> envEntity = (Map<String, Object>) entry.getValue();
            List<Map<String,String>> items = (List<Map<String, String>>) envEntity.get("items");
            if(entry.getKey().equalsIgnoreCase(GLOBAL_ENV_TOKEN)){
                for(Map<String,String> map:items){
                    for(Map.Entry<String,String> itemEntry:map.entrySet()){
                        addGlobalEnv(itemEntry.getKey(),itemEntry.getValue());
                    }
                }
            }else{
                for(Map<String,String> map:items){
                    for(Map.Entry<String,String> itemEntry:map.entrySet()){
                        addCustomEnv(entry.getKey(),itemEntry.getKey(),itemEntry.getValue());
                    }
                }
            }
        }
    }

    public static void addGlobalEnv(String key,String value){
        GLOBAL_ENV.put(key,value);
    }

    public static void addCustomEnv(String envKey,String key,String value){
        CustomEnv env = CUSTOM_ENV_MAPPING.get(envKey);
        if(env!=null){
            env.put(key,value);
        }else{
            env = new CustomEnv(envKey);
            env.put(key,value);
            CustomEnv oldValue = CUSTOM_ENV_MAPPING.putIfAbsent(envKey,env);
            if(oldValue!=null){//当oldValue为空时，表示时第一次添加，则符合当前情况，说明没有其他线程处理，如果返回不会空，说明有其他现场在操作相同的变量，则只需讲当前的加进入即可
                oldValue.put(key,value);
            }
        }
    }

    public static String getEnv(String key){
        String value = GLOBAL_ENV.get(key);//默认先从全局获取变量
        String envValue=null;
        for(CustomEnv env : CUSTOM_ENV_MAPPING.values()){
            envValue = env.get(key);
            if(!StringUtils.isEmpty(envValue)){
                return envValue;
            }
        }
        return envValue==null?value:envValue;
    }

    public static String getEnv(String key,String envKey){
        String value = GLOBAL_ENV.get(key);//默认先从全局获取变量
        CustomEnv env = CUSTOM_ENV_MAPPING.get(envKey);
        String envValue=null;
        if(env!=null){
            envValue = env.get(key);
        }
        return envValue==null?value:envValue;
    }




    static class GlobalEnv extends  SmartPostEnvironment{

        public GlobalEnv(){
            this(EnvType.GLOBAL);
        }

        protected GlobalEnv(EnvType type) {
            super(type);
        }
    }

    static  class  CustomEnv extends  SmartPostEnvironment{

        private String envId;

        public CustomEnv(String envId){
            this(EnvType.CUSTOM);
            this.envId = envId;
        }

        public String getEnvId() {
            return envId;
        }

        protected CustomEnv(EnvType type) {
            super(type);
        }
    }
}
