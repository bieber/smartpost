/**
 * 
 */
package com.bieber.smartpost.httpcore;

import com.bieber.smartpost.util.Utils;
import com.jbeer.framework.JBeer;
import com.jbeer.framework.utils.StringUtils;
import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author bieber
 * 响应实体
 */
public class Response {

    private static final String RESPONSE_CONTENT_TYPE_TOKEN="Content-Type";

	private Header header;
	
	private InputStream respInput;
	
	private String content;

    private String responseCode;

    private String contentType ;

    private OutputStream outputStream;
    //标示当前的response是否时空的，如果是空的，则说明该response还没有进行请求
    private volatile boolean isEmpty=true;

    public Response(InputStream input,String contentType) throws IOException {
        respInput=input;
        this.contentType=contentType;
        getResponseContent();
        isEmpty=false;
    }
    
    public Response(){

    }

    public void setHeader(Header header){
        this.header= header;
        this.contentType = header.get(RESPONSE_CONTENT_TYPE_TOKEN);
    }
    
    public Response(String responseCode,String responseContent,Header header){
        this.responseCode = responseCode;
        this.content = responseContent;
        this.header = header;
    }

    public boolean isEmpty() {
        return isEmpty;
    }


    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public String getContentType() {
        return contentType;
    }

    public void copyFrom(Response response) throws IOException {
        this.header = response.getHeader();
        this.contentType = response.getContentType();
        this.content = response.getResponseContent();
        this.responseCode = response.getResponseCode();
        isEmpty=false;
    }

    public Header getHeader(){
		return header;
	}

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }


	
	public String getResponseContent() throws IOException{
		if(content==null&&respInput!=null){
			ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int offset=-1;
			while((offset=respInput.read(buffer, 0, 1024))>0){
					byteBuffer.write(buffer, 0, offset);
			}
			respInput.close();
            if(!StringUtils.isEmpty(contentType)&&contentType.startsWith("image")){
                content ="data:"+contentType.split(";")[0]+";base64,"+Base64.encodeBase64(byteBuffer.toByteArray());
            }else{
			    content = new String(byteBuffer.toByteArray(), JBeer.getApplicationEncode());
            }
		}
		return content;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("Response header:\n");
		sb.append(header!=null?header.toString():"");
		sb.append("Response body:\n");
		try {
			sb.append(getResponseContent());
		} catch (IOException e) {
            Utils.COMMONS_LOGGER.debug("failed to string",e);
        }
		return sb.toString();
	}
	
	
	
}
