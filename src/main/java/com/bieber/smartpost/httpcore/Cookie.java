package com.bieber.smartpost.httpcore;

import com.jbeer.framework.utils.StringUtils;

/**
 * Created by bieber on 14-10-31.
 */
public class Cookie {

    private String expires;

    private String maxAge;

    private String path;

    private String domain;

    private String version;

    private String name;

    private String value;

    private static final String PATH_TOKEN="path";

    private static final String DOMAIN_TOKEN="domain";

    private static final String MAX_AGE_TOKEN="max-age";

    private static final String EXPIRES_TOKEN="expires";

    private static final String VERSION_TOKEN="version";

    public Cookie(){

    }

    public Cookie(String cookieStr){
        if(!StringUtils.isEmpty(cookieStr)){
            String[] splits = cookieStr.split(";");
            for(String item:splits){
                    int index = item.indexOf("=");
                    if(index<0){
                        continue;
                    }
                    String key =item.substring(0,index);
                    String value=item.substring(index+1);
                    if(!StringUtils.isEmpty(key)){
                        key=key.trim();
                        if(key.toLowerCase().equals(PATH_TOKEN)){
                            setPath(value.trim());
                        }else if(key.toLowerCase().equals(DOMAIN_TOKEN)){
                            setDomain(value.trim());
                        }else if(key.toLowerCase().equals(MAX_AGE_TOKEN)){
                            setMaxAge(value.trim());
                        }else if(key.toLowerCase().equals(EXPIRES_TOKEN)){
                            setExpires(value.trim());
                        }else if(key.toLowerCase().equals(VERSION_TOKEN)){
                            setVersion(value.trim());
                        }else{
                            setName(key);
                            String tempValue = value.trim();
                            if(!tempValue.startsWith("\"")||!tempValue.endsWith("\"")){
                                tempValue="\""+tempValue+"\"";
                            }
                            setValue(tempValue);
                        }
                    }
            }
        }
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(String maxAge) {
        this.maxAge = maxAge;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cookie cookie = (Cookie) o;

        if (name != null ? !name.equals(cookie.name) : cookie.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Cookie{" +
                "expires='" + expires + '\'' +
                ", maxAge='" + maxAge + '\'' +
                ", path='" + path + '\'' +
                ", domain='" + domain + '\'' +
                ", version='" + version + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}


