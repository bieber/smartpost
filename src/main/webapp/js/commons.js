
//绑定键值对添加添加事件
function bindAddKV(){
    $(".add-key-value").unbind("click");
    $(".remove-key-value").unbind("click");
    $(".add-key-value").bind("click",function(){
        $(this).parents(".row:first").parent().append(template("keyValueNoHeadTpl",{}));
        bindAddKV();
    });
    $(".remove-key-value").bind("click",function(){
        var parentRow = $(this).parents(".row:first");
        parentRow.prev().remove();
        parentRow.remove();
    });
}

function copyObject(object){
    var copyObject = {};
    for(var pro in object){
        if(object.hasOwnProperty(pro)){
            copyObject[pro]=object[pro];
        }
    }
    return copyObject;
}
var filterParams=function(entityJson){
    var value = entityJson.params;
    if(entityJson.parametersType==='json'){
        try {
            var json = JSON.parse(value);
            value = JSON.stringify(json);
        }catch(e){
            var errorDialog = new dialog({
                content:"内容不符合json格式！"
            })
            errorDialog.open();
            throw e;
        }
    }else if(entityJson.parametersType==='xml'){
        try {
            value = "<div>" + value + "</div>"
            var xml = $.parseXML(value);
            xml = $(xml);
            value = xml.html();
        }catch(e){
            var errorDialog = new dialog({
                content:"内容不符合xml/html格式！"
            })
            errorDialog.open();
            throw e;
        }
    }
    entityJson.params=value;
}