template.helper("getMapKey",function(map){
    if(!map){
        return "";
    }
    for(var pro in map){
        return pro;
    }
});
template.config("escape",false);
template.helper("getMapValue",function(map){
    if(!map){
        return "";
    }
    for(var pro in map){
        return map[pro];
    }
});

template.helper("toString",function(val,def){
    return val?val:def;
});
template.helper("formatText",function(val,type){
    switch (type){
        case 'raw':return val;
        case 'json':{
            try{
                JSON.parse(val);
            }catch(e){
                return val;
            }
            return vkbeautify.json(val,4);}
        case 'xml':return vkbeautify.xml(val,4);
        case 'sql':return vkbeautify.sql(val,4);
        default :{
            try{
            JSON.parse(val);
            }catch(e){
                return val;
            }
            return  vkbeautify.json(val,4);}
    }
});
template.helper("includeTemple",function(object,tplname){
    if(!object){
        return "";
    }
    var temp={};
    temp.value=object;
    return template(tplname,temp);
});
template.helper("includeString",function(val,included){
    if(val&&val.indexOf(included)>=0){
        return true;
    }
    return false;
});