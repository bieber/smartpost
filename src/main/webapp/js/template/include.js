var isIE = (navigator.userAgent.indexOf('MSIE') >= 0) && (navigator.userAgent.indexOf('Opera') < 0);
(function(_window){
	_window.templateInclude=function(filePaths){
		var me=this;
	for(var i=0;i<filePaths.length;i++){
		var filePath = filePaths[i];
		$.ajax({
			url:filePath,
			type:"get",
			dataType:"text",
			async:false,
			error:function(jqXHR,status,error){
				alert(status+" "+error);
			},
			success:function(data){
			  me.loadTemplate(data);  
			}
		});
		}
	};
	
	templateInclude.prototype.loadTemplate=function(template){
		var $tempDom ;
		if(isIE){
			$tempDom=$($.parseXML(template));
		}else{
			$tempDom=$(template);
		}
		  
		$tempDom.find("template").each(function(){
				var templateId = $(this).attr("id");
				var templateContent;
				if(isIE){
					templateContent=this.firstChild.xml;
				}else{
					templateContent=$(this).html();
				}  
				var templateScript = $("<script/>");
				templateScript.attr("id",templateId);
				templateScript.attr("type","text/html");
				var templateScript='<script id="'+templateId+'" type="text/html">';
				templateScript+=templateContent;
				templateScript+='</script>';
				$("body").append(templateScript);
				
		});
	};
	 
})(window);
