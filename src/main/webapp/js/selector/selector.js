(function(_window,$){
	var selector = function(option){
		var defaultOption={
				selector:undefined,
				onChange:undefined,
                initData:undefined,
                initValue:undefined,
                defaultLabel:"请选择"
		}
		this.option=this._extend(defaultOption,option);
		this._init();
	};
	_window.selector=selector;
	selector.prototype._extend=function(defaultOption,option){
			for(var pro in option){
				if(option.hasOwnProperty(pro)){
					defaultOption[pro]=option[pro];
				}
			}
			return defaultOption;
	}
    selector.prototype.refreshOptions=function(data){
        var html = "";
        var firstValue;
        for(var pro in data){
            if(!firstValue){
                firstValue=pro;
            }
            html+='<li><a href="#'+pro+'" value="'+pro+'">'+data[pro]+'</a></li>';
        }
        if(firstValue){
            this.option.valueDomain.val(firstValue);
            this.option.label.text(data[firstValue]);
        }else{
            this.option.valueDomain.val("");
            this.option.label.text(this.option.defaultLabel);
        }
        this.option.label.attr("title",this.option.label.text());
        this.option.ul.html(html);
        this._bindEvent();
    }
	selector.prototype._init=function(){
        this.option.button = this.option.selector.find("button :first")
		this.option.label=this.option.selector.find("button b:first");
        this.option.label.addClass("select-label");
        this.option.label.attr("title",this.option.label.text());
		this.option.ul=this.option.selector.find("ul:first");
        this.option.ul.css({maxHeight:200,overflow:'auto'});
        this.option.valueDomain=this.option.selector.find("input:first");
		var me = this;
        if(me.option.initData){
            var html = "";
            for(var pro in me.option.initData){
                html+='<li><a href="#'+pro+'" value="'+pro+'">'+me.option.initData[pro]+'</a></li>';
            }
            me.option.ul.append(html);
        }
        me._bindEvent();
        //me._bindAdapterEvent();
	}
   /* selector.prototype._bindAdapterEvent=function(){
        var me =this;
        this.option.button.bind("click",function(){
            var windowWidth = window.document.body.clientWidth;
            var windowHeight = window.document.body.clientHeight;
            var ulLeft = me.option.ul.offset().left;
            var ulTop = me.option.ul.offset().top;
            var ulWidth = me.option.ul.width();
            var ulHeight = me.option.ul.height();
            var buttonTop = me.option.ul.offset().top;
            if(ulLeft+ulWidth>windowWidth){
                me.option.ul.offset({left:(windowWidth-ulWidth)});
            }
            if(ulHeight+ulTop>windowHeight){
                me.option.ul.offset({top:buttonTop-ulHeight});
            }

        });
    }*/
    selector.prototype.rebindEvent = function(){
        this.option.ul.find("li").unbind("click");
        this._bindEvent();
    }
    selector.prototype._bindEvent=function(){
        var me = this;
        if(me.option.initValue){
            me.option.valueDomain.val(me.option.initValue);
        }
        me.option.ul.find("a[value='"+this.option.valueDomain.val()+"']").addClass("selected");
        me.option.ul.find("li").bind("click",function(){
            var currentLi = $(this);
            var a  = currentLi.find("a");
            if(me.option.valueDomain.val()!= a.attr("value")){
                var changed = true;
                if(me.option.onChange){
                    changed=me.option.onChange({target:me.option.selector,selectedLabel:a.text(),selectedHref:a.attr("href"),value:a.attr("value")});
                }
                if(changed){
                    me.option.ul.find(".selected").removeClass("selected");
                    currentLi.addClass("selected");
                    me.option.label.text(a.text());
                    me.option.label.attr("title",me.option.label.text());
                    me.option.valueDomain.val(a.attr("value"));
                }
            }
        });
    }
})(window,$);