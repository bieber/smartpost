(function(){
    var reg = new RegExp("[@]{1}[a-z]{1,}[\|]{1}[\u4e00-\u9fa5]{1,}[\|]{1}[A-Z]{1,}[\|]{1}[a-z]{1,}[\|]{1}[a-z]{1,}[\|]{1}[0-9]{1,}","gi");
    var stations = station_names.match(reg);
    var stationsMap={};
    var SMART_POST_REMOTE_COOKIE_TOKEN='SMART_POST_REMOTE_COOKIE_TOKEN_';
    for(var i=0;i<stations.length;i++){
        var item = stations[i];
        var items = item.split("|");
        var stationMap={};
        stationMap.nativeCode=items[0];
        stationMap.name=items[1];
        stationMap.code=items[2];
        stationMap.pinyin=items[3];
        stationMap.simpleCode=items[4];
        stationMap.index=items[5];
        stationsMap[stationMap.code]=stationMap;
    }
    window.stationsMap=stationsMap;
    var generate12306Cookie=function(params){
        for(var i=0;i<params.length;i++){
            var param = params[i];
            for(var pro in param){
                if(pro=='leftTicketDTO.from_station'){
                    var stationName = stationsMap[param[pro]].name+",";
                    setCookie(SMART_POST_REMOTE_COOKIE_TOKEN+"_jc_save_fromStation",escape(stationName)+param[pro]);
                }else  if(pro=='leftTicketDTO.to_station'){
                    var stationName = stationsMap[param[pro]].name+",";
                    setCookie(SMART_POST_REMOTE_COOKIE_TOKEN+"_jc_save_toStation",escape(stationName)+param[pro]);
                }else if(pro=='leftTicketDTO.train_date'){
                    setCookie(SMART_POST_REMOTE_COOKIE_TOKEN+"_jc_save_toDate",param[pro]);
                    setCookie(SMART_POST_REMOTE_COOKIE_TOKEN+"_jc_save_fromDate",param[pro]);
                    setCookie(SMART_POST_REMOTE_COOKIE_TOKEN+"_jc_save_wfdc_flag",'dc');
                }
            }
        }
    }

    function setCookie(name,value){
        document.cookie=name+"="+value;
    }
    window.generate12306Cookie=generate12306Cookie;
})();