;(function(){
    var loading= function (mode) {
        if(mode<=0){
            $("#loading_mask").remove();
            $("#loading_dialog").remove();
        }else{
            var body = $("body");
            generateMask();
            body.append(generateLoading());
            var loading = $("#loading_dialog");
            var position = generatePosition();
            loading.offset({
                left : position.left,
                top:position.top
            });
        }
    }
    window.loading=loading;
    var generatePosition = function() {
        var $dialog = $("#loading_dialog");
        var bodySize = getCurrentVistableSize();
        var width = $dialog.width();
        var height = $dialog.height();
        var top = bodySize.height / 2 - height / 2;
        var left = bodySize.width / 2 - width / 2;
        return {
            top : top,
            left : left
        };
    };
    var getCurrentVistableSize = function() {
        return {
            width : window.document.body.clientWidth,
            height : window.document.body.clientHeight
        };
    };
    var generateLoading = function() {
        var dialogHtml = '<div style="position:absolute;width:60px;height:60px;opacity:1; z-index: 100001;" id="loading_dialog"><img src="img/loading.gif"></div>';
        return dialogHtml;
    }
    var generateMask = function() {
        var body = $("body");
        var bodySize =  getCurrentVistableSize();
        var maskHtml = '<div id="loading_mask" style="width:'
            + bodySize.width
            + 'px;height:'
            + bodySize.height
            + 'px;opacity:0;filter:alpha(opacity=0);background-color:#333;z-index:1000;top:0;left:0;position:absolute;"></div>';
        body.append(maskHtml);
    };
})();