(function(_window, $) {
	var dialog = function(option) {
		var defaultOption = {
			title : "警告",
			content : undefined,
			onclose : undefined,
			isConfirm : false,
			onOk : undefined,
			onCancel : undefined,
			ok : "确认",
			cancel : "取消",
			dragable:true,
			mode : true
		};
		this.option = this._extendOption(defaultOption, option);
		this.id = this._generateId();
	};
	_window.dialog = dialog;

	dialog.prototype._extendOption = function(defaultOption, option) {
		for ( var pro in option) {
			if (option.hasOwnProperty(pro)) {
				defaultOption[pro] = option[pro];
			}
		}
		return defaultOption;
	};
	dialog.prototype.close = function() {
		var $dialog = $("#" + this.id + "_dialog ");
		var $mask = $("#" + this.id + "_mask");
		if (this.option.onclose) {
			this.option.onclose(this.id);
		}
		$dialog.animate({
			top : 0,
			opacity : 0
		}, 'slow', 'linear', function() {
			$dialog.remove();
			$mask.remove();
		});
	}

	dialog.prototype._getCurrentDialogposition = function() {
		var $dialog = $("#" + this.id + "_dialog ");
		return $dialog.offset();
	}

	dialog.prototype._getCurrentMouseposition = function(event) {
		var e = event || window.event;
		return {
			left : e.clientX,
			top : e.clientY
		};
	}

	dialog.prototype._resetDialogPosition=function(event){
		var $dialog = $("#" + this.id + "_dialog ");
		var me=this;
		var currentMousePosition = me._getCurrentMouseposition(event);
		$dialog.offset({
			left : (parseInt(currentMousePosition.left)
					+ parseInt(me.mousedownDialogPosition.left) - parseInt(me.mousedownPosition.left)),
			top : (parseInt(currentMousePosition.top)
					+ parseInt(me.mousedownDialogPosition.top) - parseInt(me.mousedownPosition.top))
		});
	}
	dialog.prototype._drag=function(){
		var $dialog = $("#" + this.id + "_dialog ");

		var me = this;
		$dialog.find(".panel-heading").bind("mousedown", function(event) {
			me.isMouseDown = true;
			me.mousedownPosition = me._getCurrentMouseposition(event);
			me.mousedownDialogPosition = me._getCurrentDialogposition();
		});
		$dialog.find(".panel-heading").bind("mousemove",function(event) {
							if (me.isMouseDown) {
								me._resetDialogPosition(event);
							}
			$dialog.find(".panel-heading").css({
				cursor : 'move'
			});
						});
		$dialog.find(".panel-heading").bind( "mouseup",function(event) {
					if (me.isMouseDown) {
						me._resetDialogPosition(event);
						me.isMouseDown=false;
						$dialog.find(".panel-heading").css({
						cursor : 'default'
						});
					}
				});
	}
	dialog.prototype._bindEvent = function() {
		var $dialog = $("#" + this.id + "_dialog ");

		var me = this;
		$dialog.find(".glyphicon-remove").bind("click", function() {
			me.close();
		});
	
		if(me.option.dragable){
			me._drag();
		}
		$dialog.find(".btn-primary").bind("click", function() {
			if (me.option.onOk) {
				me.option.onOk(me.id);
			}
			if (!me.option.isConfirm) {
				me.close();
			}
		});
		$dialog.find(".btn-warning").bind("click", function() {
			if (me.option.onCancel) {
				me.option.onCancel(me.id);
			}
			me.close();
		});
	}

	dialog.prototype.open = function() {
		var body = $("body");
		if (this.option.mode) {
			this._generateMask(this.id);
		}

		body.append(this._generateDialog(this.id));
		var $dialog = $("#" + this.id + "_dialog");
		$dialog.css({
			zIndex : 100001
		});
		var position = this._generatePosition();
		$dialog.offset({
			left : position.left
		});
		$dialog.animate({
			top : position.top,
			opacity : 1
		}, 'slow', 'linear');
		this._bindEvent();
	};
    dialog.prototype.rePosition = function() {
        var position = this._generatePosition();
        var $dialog = $("#" + this.id + "_dialog");
        $dialog.animate({
            top : position.top,
            left:position.left,
            opacity : 1
        }, 'slow', 'linear');
    };
	dialog.prototype._generatePosition = function() {
		var $dialog = $("#" + this.id + "_dialog");
		var bodySize = this._getCurrentVistableSize();
		var width = $dialog.width();
		var height = $dialog.height();
		var top = bodySize.height / 2 - height / 2;
		var left = bodySize.width / 2 - width / 2;
		return {
			top : top,
			left : left
		};
	};
	dialog.prototype._generateDialog = function(id) {
		var dialogHtml = '<div  style="top:0px;position:absolute;opacity:0;" id="'
				+ id + '_dialog"><div class="panel panel-primary">';
		dialogHtml += ' <div class="panel-heading" >'
				+ this.option.title
				+ '<span class="glyphicon glyphicon-remove" style="float: right;cursor: pointer;"></span></div>';
		dialogHtml += ' <div class="panel-body" style="min-width: 250px;text-align: center;">';
		dialogHtml += this.option.content;
		dialogHtml += ' </div>';
		if (this.option.isConfirm) {
			dialogHtml += ' <div class="panel-footer"> <div class="row"><div class="col-md-6"><button type="button" class="btn btn-primary" style="float:right;"><span class="glyphicon glyphicon-ok"></span> '
					+ this.option.ok
					+ '</button></div><div class="col-md-6"> <button type="button" class="btn btn-warning "> <span class="glyphicon glyphicon-remove"></span>'
					+ this.option.cancel + ' </button></div></div></div>'
		} else {
			dialogHtml += ' <div class="panel-footer"> <div class="row"><div class="col-md-12" style="text-align: center;"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> '
					+ this.option.ok + '</button></div></div></div>'
		}
		dialogHtml += '</div>';
		return dialogHtml;
	}
	dialog.prototype._generateMask = function(id) {
		var body = $("body");
		var bodySize = this._getCurrentVistableSize();
		var maskHtml = '<div id="'
				+ id
				+ '_mask" style="width:'
				+ bodySize.width
				+ 'px;height:'
				+ bodySize.height
				+ 'px;opacity:0.2;filter:alpha(opacity=20);background-color:#333;z-index:1000;top:0;left:0;position:absolute;"></div>';
		body.append(maskHtml);
	};
	dialog.prototype._generateId = function() {
		return Math.uuid();
	};
	dialog.prototype._getCurrentVistableSize = function() {
		return {
			width : window.document.body.clientWidth,
			height : window.document.body.clientHeight
		};
	};
	dialog.prototype._getCurrentPageSize = function() {
		return {
			width : window.document.body.scrollWidth,
			height : window.document.body.scrollHeight
		};
	};
})(window, $);